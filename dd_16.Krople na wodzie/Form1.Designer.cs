﻿namespace dd_16.Krople_na_wodzie
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxLewy = new System.Windows.Forms.ComboBox();
            this.comboBoxSrodkowy = new System.Windows.Forms.ComboBox();
            this.comboBoxPrawy = new System.Windows.Forms.ComboBox();
            this.pictureBoxRysunek = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.buttonCzysc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRysunek)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxLewy
            // 
            this.comboBoxLewy.FormattingEnabled = true;
            this.comboBoxLewy.Location = new System.Drawing.Point(451, 23);
            this.comboBoxLewy.Name = "comboBoxLewy";
            this.comboBoxLewy.Size = new System.Drawing.Size(121, 21);
            this.comboBoxLewy.TabIndex = 0;
            // 
            // comboBoxSrodkowy
            // 
            this.comboBoxSrodkowy.FormattingEnabled = true;
            this.comboBoxSrodkowy.Location = new System.Drawing.Point(451, 67);
            this.comboBoxSrodkowy.Name = "comboBoxSrodkowy";
            this.comboBoxSrodkowy.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSrodkowy.TabIndex = 1;
            // 
            // comboBoxPrawy
            // 
            this.comboBoxPrawy.FormattingEnabled = true;
            this.comboBoxPrawy.Location = new System.Drawing.Point(451, 114);
            this.comboBoxPrawy.Name = "comboBoxPrawy";
            this.comboBoxPrawy.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPrawy.TabIndex = 2;
            // 
            // pictureBoxRysunek
            // 
            this.pictureBoxRysunek.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxRysunek.Name = "pictureBoxRysunek";
            this.pictureBoxRysunek.Size = new System.Drawing.Size(416, 321);
            this.pictureBoxRysunek.TabIndex = 3;
            this.pictureBoxRysunek.TabStop = false;
            this.pictureBoxRysunek.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxRysunek_MouseDown);
            this.pictureBoxRysunek.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxRysunek_MouseUp);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // buttonCzysc
            // 
            this.buttonCzysc.Location = new System.Drawing.Point(451, 150);
            this.buttonCzysc.Name = "buttonCzysc";
            this.buttonCzysc.Size = new System.Drawing.Size(121, 23);
            this.buttonCzysc.TabIndex = 4;
            this.buttonCzysc.Text = "Czyść";
            this.buttonCzysc.UseVisualStyleBackColor = true;
            this.buttonCzysc.Click += new System.EventHandler(this.buttonCzysc_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(448, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Lewy przycisk myszy:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(448, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Środkowy przycisk myszy:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(448, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Prawy przycisk myszy:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 345);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCzysc);
            this.Controls.Add(this.pictureBoxRysunek);
            this.Controls.Add(this.comboBoxPrawy);
            this.Controls.Add(this.comboBoxSrodkowy);
            this.Controls.Add(this.comboBoxLewy);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRysunek)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxLewy;
        private System.Windows.Forms.ComboBox comboBoxSrodkowy;
        private System.Windows.Forms.ComboBox comboBoxPrawy;
        private System.Windows.Forms.PictureBox pictureBoxRysunek;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button buttonCzysc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

