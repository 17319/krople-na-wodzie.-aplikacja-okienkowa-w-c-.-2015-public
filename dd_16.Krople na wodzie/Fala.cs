﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dd_16.Krople_na_wodzie
{
    class Fala
    {
        private TypFali typ;
        private Point srodek;
        private int promien;
        public int Promien { get { return promien; } }
        private Timer timer;

        public Fala(Point point, TypFali typFali, TimeSpan dlugoscKliku)
        {
            this.srodek = point;
            this.typ = typFali;
            timer = new Timer();
            timer.Interval = Convert.ToInt32(dlugoscKliku.TotalMilliseconds / 10);
            timer.Enabled = true;
            timer.Tick += timer_Tick;
            promien = 1;
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            if (typ != TypFali.Punkt)
            {
                promien++;
            }
        }

        internal void rysuj(Graphics g)
        {
            if (typ == TypFali.Kolo)
            {
                g.DrawEllipse(new Pen(Color.FromArgb(0,255,255), 2), srodek.X - promien, srodek.Y - promien, promien * 2, promien * 2);
            }
            else if (typ == TypFali.Kwadrat)
            {
                g.DrawRectangle(new Pen(Color.FromArgb(0, 255, 255), 2), srodek.X - promien, srodek.Y - promien, promien * 2, promien * 2);
            }
            else if (typ == TypFali.Punkt)
            {
                g.FillEllipse(new SolidBrush(Color.Yellow), srodek.X - 5, srodek.Y - 5, 5, 5);
                g.DrawEllipse(new Pen(Color.FromArgb(0, 255, 255), 2), srodek.X - 5, srodek.Y - 5, 5, 5);
            }
        }

    }
}
