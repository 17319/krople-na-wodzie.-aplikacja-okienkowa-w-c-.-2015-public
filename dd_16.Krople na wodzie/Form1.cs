﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dd_16.Krople_na_wodzie
{
    public partial class Form1 : Form
    {
        List<Fala> lista;
        Graphics g;
        DateTime start;
        public Form1()
        {
            InitializeComponent();

            lista = new List<Fala>();

            wypelnij(comboBoxLewy);
            wypelnij(comboBoxSrodkowy);
            wypelnij(comboBoxPrawy);

            pictureBoxRysunek.Image = new Bitmap(pictureBoxRysunek.Width, pictureBoxRysunek.Height);
           
            g = Graphics.FromImage(pictureBoxRysunek.Image);
            g.Clear(Color.FromArgb(0,0,255));
        }

        private void wypelnij(ComboBox cb)
        {
            cb.Items.Add(TypFali.Kolo);
            cb.Items.Add(TypFali.Kwadrat);
            cb.Items.Add(TypFali.Punkt);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            
            rysuj();
        }

        private void rysuj()
        {
            g.Clear(Color.FromArgb(0, 0, 255));
            foreach (Fala f in lista)
            {
                f.rysuj(g);
            }
            // usuwamy fale których nie widać
            lista = lista.Where(f => f.Promien <= Math.Sqrt(pictureBoxRysunek.Width * pictureBoxRysunek.Width
                + pictureBoxRysunek.Height * pictureBoxRysunek.Height)).ToList();
            pictureBoxRysunek.Refresh();

        }

        private void pictureBoxRysunek_MouseDown(object sender, MouseEventArgs e)
        {
            start = DateTime.Now;
        }

        private void pictureBoxRysunek_MouseUp(object sender, MouseEventArgs e)
        {
            TimeSpan dlugoscKliku = DateTime.Now - start;

            if (e.Button == MouseButtons.Left && comboBoxLewy.SelectedItem != null)
            {
                lista.Add(new Fala(e.Location, (TypFali)comboBoxLewy.SelectedItem, dlugoscKliku));
            }
            else if (e.Button == MouseButtons.Middle && comboBoxSrodkowy.SelectedItem != null)
            {
                lista.Add(new Fala(e.Location, (TypFali)comboBoxSrodkowy.SelectedItem, dlugoscKliku));
            }
            else if (e.Button == MouseButtons.Right && comboBoxPrawy.SelectedItem != null)
            {
                lista.Add(new Fala(e.Location, (TypFali)comboBoxPrawy.SelectedItem, dlugoscKliku));
            }
            else
            {
                MessageBox.Show("Nie wybrano odpowiedniej opcji.");
            }
            
            rysuj();
        }

        private void buttonCzysc_Click(object sender, EventArgs e)
        {
            lista.Clear();
        }


    }
}

